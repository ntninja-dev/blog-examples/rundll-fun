#include <windows.h>

extern "C" {

	__declspec(dllexport)
	VOID __fastcall 
	MyExport(HWND hWnd, HINSTANCE hInst, LPSTR CommandLine, INT nShowCmd)
	{
		UNREFERENCED_PARAMETER(hWnd);
		UNREFERENCED_PARAMETER(hInst);
		UNREFERENCED_PARAMETER(nShowCmd);
		 
		MessageBoxA(NULL, CommandLine, "RUNDLL-FUN", MB_OK);
		return;
	}

}